# <img width="50" src="https://lh3.googleusercontent.com/sGQFOfOfbA5uYNRc5eoeMT9_KxL4ofl6YFEKQdnyZrCxgc1UOwku0PVgdudgYMro_xsv"> Gamee Games Hack

## ℹ️ Disclaimer

First of all, I am not responsible for the misuse that is given to the following information because I publish it for an informative and educational purpose.

## 💡 Introduction

After investigating between game and game I have found the lines to modify to hack the score as of February 2022.

Currently this bug just works on the following 4 games:

- [Neon Blaster 2](#neon-blaster-2)
- [Hausschwein Run](#hausschwein-run)
- [Color Hit](#color-hit)
- [Bastket Boy](#basket-boy)

## 🔨 Usage

You just need a browser that has its developer tools enabled, a Chrome-like browser.

## ⚠️ Advise

I RECOMMEND NOT TO GET EXCITED WITH THE FIGURES BY WHICH THE SCORE IS GOING TO BE MULTIPLIED, GAME IF IT DETECTS VERY STRANGE SCORES, BAN THE ACCOUNTS.

# List of available games to hack

## Neon Blaster 2

Open the game link on your browser then play one game (It is just for the browser to load all the necessary game files)

When you are done playing this game open Developer Tools `Ctr + Mayus + I`.

Click on Console, then press `Ctrl + Shift + F` to search the word `score`.

Then double click on `c2runtime.js` file.

Search for line `25122`  `gamee.updateScore(score, boolGhostSign, error => {`

Edit the `score` text, add a number to multiply over your score followed by score, example: `score*260` so the result will be:

`gamee.updateScore(score*260, boolGhostSign, error => {`

Then press `Ctrl + S`, close the Developer Tools tabs and play beating your friends scores.

  
## Hausschwein Run

Open the game link on your browser then play one game (It is just for the browser to load all the necessary game files)

When you are done playing this game open Developer Tools `Ctr + Mayus + I`.

Click on Console, then press `Ctrl + Shift + F` to search the word `score`.

Then double click on `tph_js_extension.js` file.

Search for line `223`  `gamee.score = score;`

Edit the `score` text, add a number to multiply over your score followed by score, example: `score*2` so the result will be:

`gamee.score = score*2;`

Then press `Ctrl + S`, close the Developer Tools tabs and play beating your friends scores.

  
# Color Hit

Open the game link on your browser then play one game (It is just for the browser to load all the necessary game files)

When you are done playing this game open Developer Tools `Ctr + Mayus + I`.

Click on Console, then press `Ctrl + Shift + F` to search the word `score`.

Then double click on `c2runtime.js` file.

Search for line `24192`  `gamee.updateScore(score, boolGhostSign, (error) => {`

Edit the `score` text, add a number to multiply over your score followed by score, example: `score*2` so the result will be:

`gamee.updateScore(score*2, boolGhostSign, (error) => {`

Then press `Ctrl + S`, close the Developer Tools tabs and play beating your friends scores.


## Basket Boy

Open the game link on your browser then play one game (It is just for the browser to load all the necessary game files)

When you are done playing this game open Developer Tools `Ctr + Mayus + I`.

Click on Console, then press `Ctrl + Shift + F` to search the word `score`.

Then double click on `tph_js_extension.js` file.
  
Search for line `334`  `gamee.updateScore(score);`

Edit the `score` text, add a number to sum over your score followed by score, example: `score+1` so the result will be:

`gamee.updateScore(score+1);`

Then press `Ctrl + S`, close the Developer Tools tabs and play beating your friends scores.




>  *Last Update: 02/02/2022.*
